import * as functions from "firebase-functions";
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
export const helloWorld = functions.https.onRequest((request, response) => {
  response.send("Hello from Firebase!");
});

export const confirmationPayu = functions.https.onRequest(
  (request, response) => {
    if (request.body) {
      console.log("confirmationPayu", request.body);
      const db = admin.database();
      const uid = request.body.extra1;
      const ref = db.ref("users");
      const usersRef = ref.child(`${uid}/confirmationPayu/`);
      if (request.body.response_message_pol == "APPROVED") {
      } else {
      }
      usersRef.set(request.body);
      response.send("Hello from confirmationPayu!");
    }
  }
);

export const responsePayu = functions.https.onRequest((request, response) => {
  if (request.body) {
    console.log("responsePayu", request.body);
    const db = admin.database();
    const uid = request.body.extra1;
    const ref = db.ref("users");
    if (request.body.response_message_pol == "APPROVED") {
    }
    const usersRef = ref.child(`${uid}/responsePayu/`);
    usersRef.set(request.body);
    response.send("Hello from responsePayu!");
  }
});
