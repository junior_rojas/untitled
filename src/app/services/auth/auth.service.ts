import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { BehaviorSubject } from "rxjs";
import { Platform } from "@ionic/angular";
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "@angular/fire/database";
import AuthProvider = firebase.auth.AuthProvider;

const TOKEN_KEY = "auth-token";
const ROL_KEY = "rol-key";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  authenticationState = new BehaviorSubject(false);
  private user: firebase.User;
  public token;
  public role: any;
  _signIn: boolean;
  _signUp: boolean;
  _recoverPassword: boolean;
  constructor(
    private storage: Storage,
    public db: AngularFireDatabase,
    private plt: Platform,
    public afAuth: AngularFireAuth
  ) {
    this.login();
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  async login() {
    await this.afAuth.authState.subscribe(async user => {
      if (user.uid) {
        this.user = user;
        await this.storage.set(TOKEN_KEY, user.uid).then(async () => {
          this.token = user.uid;
          await this.getProfile(user.uid);
          setTimeout(() => {
            this.authenticationState.next(true);
          }, 1000);
        });
      }
    });
  }
  getProfile(uid) {
    if (uid) {
      return new Promise(async resolve => {
        const url = `/users/${uid}/profile`;
        this.db
          .object(url)
          .valueChanges()
          .subscribe(async profile => {
            await this.storage.set("profile", profile);
            resolve();
          });
      });
    }
  }

  signInWithEmail(credentials) {
    return this.afAuth.auth.signInWithEmailAndPassword(
      credentials.email,
      credentials.password
    );
  }

  signInWithGoogle() {
    console.log("Sign in with google");
    return this.oauthSignIn(new firebase.auth.GoogleAuthProvider());
  }

  private oauthSignIn(provider: AuthProvider) {
    if (!(<any>window).cordova) {
      return this.afAuth.auth.signInWithPopup(provider);
    } else {
      return this.afAuth.auth.signInWithRedirect(provider).then(() => {
        return this.afAuth.auth
          .getRedirectResult()
          .then((result: any) => {
            // This gives you a Google Access Token.
            // You can use it to access the Google API.
            let token = result.credential.accessToken;
            // The signed-in user info.
            let user = result.user;
            console.log(token, user);
          })
          .catch(function(error) {
            // Handle Errors here.
            alert(error.message);
          });
      });
    }
  }

  createUserWithEmailAndPassword(credentials) {
    return this.afAuth.auth.createUserWithEmailAndPassword(
      credentials.email,
      credentials.password
    );
  }

  recoverPassword(credentials) {
    return this.afAuth.auth.sendPasswordResetEmail(credentials.email);
  }

  async addProfile(profile) {
    await this.checkToken();
    await setTimeout(() => {
      const uid = this.token;
      const itemsRef = this.db.object(`users/${uid}/profile`);
      itemsRef.set(profile);
    }, 2000);
  }

  async setRol(rol) {
    return new Promise(async resolve => {
      await this.storage.set(ROL_KEY, rol).then(async () => {
        resolve();
      });
    });
  }

  getAvatar(uid) {
    const url = `/users/${uid}/avatar`;
    this.db
      .object(url)
      .valueChanges()
      .subscribe(a => {
        this.setAvatar(a);
      });
  }

  setAvatar(img) {
    return new Promise(async resolve => {
      await this.storage.set("avatar", img).then(async () => {
        resolve();
      });
    });
  }

  async checkRol() {
    await this.storage.get(ROL_KEY).then(res => {
      console.log(res);
      return res;
    });
  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.token = res;
        this.authenticationState.next(true);
      }
    });
  }

  async getToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      return res;
    });
  }

  checkProfile() {
    let profile;
    this.storage.get("profile").then(res => {
      profile = res;
    });
    return profile;
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.storage.remove(ROL_KEY);
      this.storage.clear();
      this.afAuth.auth.signOut();
      this.authenticationState.next(false);
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

  isAdmin(uid) {
    return new Promise(resolve => {
      this.db
        .object(`users/${uid}/rol`)
        .valueChanges()
        .subscribe(rol => {
          if (rol) {
            resolve(rol);
          } else {
            resolve(0);
          }
        });
    });
  }

  /**
   *
   * @param param Estatus de vista
   */
  async changeView(param: number) {
    switch (param) {
      case 1:
        this._signIn = true;
        this._signUp = false;
        this._recoverPassword = false;
        break;
      case 2:
        this._signIn = false;
        this._signUp = true;
        this._recoverPassword = false;
        break;
      case 3:
        this._signIn = false;
        this._signUp = false;
        this._recoverPassword = true;
        break;

      default:
        this._signIn = true;
        this._signUp = false;
        this._recoverPassword = false;
        break;
    }
  }
}
