import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class PaypalService {
  public payPalEnvironmentSandbox =
    "AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R";
  public payPalEnvironmentProduction = "";
}
