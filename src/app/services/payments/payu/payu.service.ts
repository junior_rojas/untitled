import { Injectable } from "@angular/core";
import { Md5 } from "ts-md5/dist/md5";

@Injectable({
  providedIn: "root"
})
export class PayuService {
  // sandbox
  public apikey = "eF3vJ2ExqWXY47Th9H8a5Pc4rZ";
  public urlRoot = "https://us-central1-untitled-jj.cloudfunctions.net";
  public actionURL =
    "https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu";
  public merchantId = "778592";
  public accountId = "785384";
  public referenceCode = Date.now();
  public signature = "";
  public currency = "COP";
  public responseUrl = this.urlRoot + "/responsePayu";
  public confirmationUrl = this.urlRoot + "/confirmationPayu";

  constructor() {}

  md5(valor): any {
    const md5 = new Md5();
    const _md5 = `${this.apikey}~${this.merchantId}~${
      this.referenceCode
    }~${valor}~${this.currency}`;
    console.log(2, _md5);
    return md5.appendStr(_md5).end();
  }
}
