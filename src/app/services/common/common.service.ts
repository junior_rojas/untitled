import { Injectable } from "@angular/core";
import {
  LoadingController,
  MenuController,
  ToastController,
  AlertController
} from "@ionic/angular";

@Injectable({
  providedIn: "root"
})
export class CommonService {
  public loading: any;
  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
    private menuCtrl: MenuController
  ) {}

  public async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      position: "top",
      duration: 3000
    });
    toast.present();
  }

  async presentAlert(text) {
    const alert = await this.alertController.create({
      message: text,
      buttons: ["OK"]
    });

    await alert.present();
  }

  public async presentLoading(text?) {
    this.loading = await this.loadingController.create({
      message: "Please wait..."
    });
    this.loading.present();
  }

  setMenuCtrl(value) {
    if (value) {
      this.menuCtrl.enable(value);
    } else {
      this.menuCtrl.enable(value);
    }
  }
}
