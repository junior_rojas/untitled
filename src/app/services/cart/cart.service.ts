import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class CartService {
  private data = [
    {
      category: "Course",
      expanded: true,
      products: [
        { id: 0, name: "Lesson 1", price: "80000" },
        { id: 1, name: "Lesson 2", price: "50000" },
        { id: 2, name: "Lesson 3", price: "90000" },
        { id: 3, name: "Lesson 4", price: "70000" }
      ]
    },
    {
      category: "Course 2",
      products: [
        { id: 4, name: "Lesson 5", price: "80000" },
        { id: 5, name: "Lesson 6", price: "60000" }
      ]
    },
    {
      category: "Course 3",
      products: [
        { id: 6, name: "Lesson 7", price: "80000" },
        { id: 7, name: "Lesson 8", price: "50000" },
        { id: 8, name: "Lesson 9", price: "90000" }
      ]
    }
  ];

  private cart = [];

  constructor() {}

  getProducts() {
    return this.data;
  }

  getCart() {
    return this.cart;
  }

  addProduct(product) {
    this.cart.push(product);
  }

  removeProduct(product) {
    this.cart.splice(product, 1);
  }
}
