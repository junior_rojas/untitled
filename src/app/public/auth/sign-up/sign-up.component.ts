import { AuthService, CommonService } from "../../../services/services";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
@Component({
  selector: "app-sign-up",
  templateUrl: "./sign-up.component.html",
  styleUrls: ["./sign-up.component.scss"]
})
export class SignUpComponent implements OnInit {
  public signUpForm: FormGroup;
  public signUpError: string;
  constructor(
    public authService: AuthService,
    public _cs: CommonService,
    public fb: FormBuilder
  ) {
    this.signUpForm = fb.group({
      name: ["", Validators.compose([Validators.required])],
      surnames: ["", Validators.compose([Validators.required])],
      country: ["", Validators.compose([Validators.required])],
      phones: ["", Validators.compose([Validators.required])],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      password: [
        "",
        Validators.compose([Validators.required, Validators.minLength(6)])
      ]
    });
  }

  ngOnInit() {}

  signUp() {
    let data = this.signUpForm.value;
    if (this.signUpForm.valid) {
      this._cs.presentLoading();
      let credentials = {
        email: data.email,
        password: data.password
      };
      this.authService.createUserWithEmailAndPassword(credentials).then(
        async user => {
          await this.authService.login();
          await this.authService.addProfile(data);
          await this._cs.loading.dismiss();
        },
        error => {
          this._cs.presentToast(error.message);
          this._cs.loading.dismiss();
        }
      );
    }
  }
}
