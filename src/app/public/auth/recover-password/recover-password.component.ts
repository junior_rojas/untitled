import { AuthService, CommonService } from "../../../services/services";
import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-recover-password",
  templateUrl: "./recover-password.component.html",
  styleUrls: ["./recover-password.component.scss"]
})
export class RecoverPasswordComponent {
  public recoverPasswordForm: FormGroup;
  constructor(
    public authService: AuthService,
    public _cs: CommonService,
    public fb: FormBuilder
  ) {
    this.recoverPasswordForm = fb.group({
      email: ["", Validators.compose([Validators.required, Validators.email])]
    });
  }

  recoverPassword() {
    let data = this.recoverPasswordForm.value;
    if (this.recoverPasswordForm.valid) {
      let credentials = {
        email: data.email
      };
      this.authService.recoverPassword(credentials).then(
        () => {
          this._cs.loading.dismiss();
          const text =
            "Te enviaremos un email con instrucciones sobre cómo restablecer tu contraseña";
          this._cs.presentAlert(text);
        },
        error => {
          this._cs.presentToast(error.message);
          this._cs.loading.dismiss();
        }
      );
    }
  }
}
