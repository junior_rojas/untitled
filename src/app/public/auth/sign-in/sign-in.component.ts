import { AuthService, CommonService } from "../../../services/services";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.scss"]
})
export class SignInComponent implements OnInit {
  public loginForm: FormGroup;
  public loginError: string;
  constructor(
    public authService: AuthService,
    public _cs: CommonService,
    public fb: FormBuilder
  ) {
    this.loginForm = fb.group({
      email: ["", Validators.compose([Validators.required, Validators.email])],
      password: [
        "",
        Validators.compose([Validators.required, Validators.minLength(6)])
      ]
    });
  }

  ngOnInit() {}

  async login() {
    let data = this.loginForm.value;
    if (this.loginForm.valid) {
      this._cs.presentLoading();
      let credentials = {
        email: data.email,
        password: data.password
      };
      this.authService.signInWithEmail(credentials).then(
        () => {
          this.authService.login();
          this._cs.loading.dismiss();
        },
        error => {
          this.loginError = error.message;
          this._cs.presentToast(error.message);
          this._cs.loading.dismiss();
        }
      );
    }
  }
}
