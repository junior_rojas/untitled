import { Component } from "@angular/core";
import { AuthService } from "../../services/services";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.page.html",
  styleUrls: ["./auth.page.scss"]
})
export class AuthPage {
  constructor(public _auth: AuthService) {
    this._auth.changeView(1);
  }
}
