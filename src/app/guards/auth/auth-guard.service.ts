import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { AuthService } from "../../services/services";
@Injectable({
  providedIn: "root"
})
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService) {}

  canActivate(): boolean {
    return this.auth.isAuthenticated();
  }
}
