import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TutorialGuard implements CanActivate {
  constructor(private storage: Storage, private router: Router) {}

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    const isComplete = await this.storage.get("tutorialComplete");

    if (!isComplete) {
      this.router.navigateByUrl("/tutorial");
    }

    return isComplete;
  }
}
