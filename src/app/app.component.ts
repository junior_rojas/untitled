import { Component } from "@angular/core";

import { Router } from "@angular/router";
import { AuthService, CommonService } from "./services/services";

import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { timer } from "rxjs/observable/timer";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  public showSplash = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public _auth: AuthService,
    public _cs: CommonService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(3000).subscribe(() => (this.showSplash = false));
      this._auth.authenticationState.subscribe(state => {
        if (state) {
          this.router.navigate(["pages", "tabs", "home"]);
          this._cs.setMenuCtrl(true);
        } else {
          this._cs.setMenuCtrl(false);
          this.router.navigate(["auth"]);
        }
      });
    });
  }
}
