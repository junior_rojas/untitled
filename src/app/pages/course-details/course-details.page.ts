import { Component, OnInit } from "@angular/core";
import { CartService, AuthService } from "../../services/services";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireDatabase } from "@angular/fire/database";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-course-details",
  templateUrl: "./course-details.page.html",
  styleUrls: ["./course-details.page.scss"]
})
export class CourseDetailsPage implements OnInit {
  public cart = [];
  public course: any;
  public token: any;
  constructor(
    private router: Router,
    private cartService: CartService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private db: AngularFireDatabase,
    public alertController: AlertController
  ) {}

  ngOnInit() {
    this.token = this.authService.token;
    this.cart = this.cartService.getCart();
    let data = this.route.snapshot.paramMap.get("data");
    this.course = JSON.parse(data);
  }

  addToCart() {
    this.cartService.addProduct(this.course);
    this.keepBuying();
  }
  async keepBuying() {
    const alert = await this.alertController.create({
      message: "seguir comprando?",
      buttons: [
        {
          text: "NO",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {
            this.openCart();
          }
        },
        {
          text: "SI",
          handler: () => {
            this.openHome();
          }
        }
      ]
    });

    await alert.present();
  }

  openCart() {
    this.router.navigateByUrl("/pages/cart");
  }

  openHome() {
    this.router.navigateByUrl("/pages/tabs/home");
  }

  async addToWishlist() {
    const alert = await this.alertController.create({
      message: "Agregar a lista de deseos?",
      buttons: [
        {
          text: "NO",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "SI",
          handler: () => {
            this.setWishList();
          }
        }
      ]
    });

    await alert.present();
  }

  setWishList() {
    this.router.navigateByUrl("/pages/tabs/wishList");
    const itemsRef = this.db.list(`users/${this.token}/_wishList`);
    itemsRef.push(this.course);
  }
}
