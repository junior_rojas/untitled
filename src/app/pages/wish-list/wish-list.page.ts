import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../../services/services";

import { AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
@Component({
  selector: "app-wish-list",
  templateUrl: "./wish-list.page.html",
  styleUrls: ["./wish-list.page.scss"]
})
export class WishListPage {
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;
  constructor(
    db: AngularFireDatabase,
    private authService: AuthService,
    private router: Router
  ) {
    this.itemsRef = db.list(`users/${this.authService.token}/_wishList`);
    this.items = this.itemsRef
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      );
  }

  openCourseDetails(r) {
    this.router.navigate(["/pages/course-details", JSON.stringify(r)]);
  }

  addItem(newName: string) {
    this.itemsRef.push({ text: newName });
  }
  updateItem(key: string, newText: string) {
    this.itemsRef.update(key, { text: newText });
  }
  deleteItem(key: string) {
    this.itemsRef.remove(key);
  }
  deleteEverything() {
    this.itemsRef.remove();
  }
}
