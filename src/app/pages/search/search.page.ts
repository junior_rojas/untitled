import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-search",
  templateUrl: "./search.page.html",
  styleUrls: ["./search.page.scss"]
})
export class SearchPage implements OnInit {
  public _query;
  public shouldShowCancel;
  items: any;
  data: any;
  constructor() {
    this.items = [
      { title: "one" },
      { title: "two" },
      { title: "three" },
      { title: "four" },
      { title: "five" },
      { title: "six" }
    ];
    this.data = this.items.slice();
  }

  ngOnInit() {}

  onInput(event) {
    if (event) {
      this.items = this.data.filter(x => {
        return x.title === event;
      });
    } else {
      this.items = this.data;
    }
  }
  onCancel(event) {}
}
