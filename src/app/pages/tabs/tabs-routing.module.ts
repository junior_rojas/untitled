import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/tabs/home",
    pathMatch: "full"
  },
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "home",
        children: [
          {
            path: "",
            loadChildren: "../home/home.module#HomePageModule"
          }
        ]
      },
      {
        path: "account",
        children: [
          {
            path: "",
            loadChildren: "../account/account.module#AccountPageModule"
          }
        ]
      },
      {
        path: "search",
        children: [
          {
            path: "",
            loadChildren: "../search/search.module#SearchPageModule"
          }
        ]
      },
      {
        path: "myCourses",
        children: [
          {
            path: "",
            loadChildren: "../my-courses/my-courses.module#MyCoursesPageModule"
          }
        ]
      },
      {
        path: "wishList",
        children: [
          {
            path: "",
            loadChildren: "../wish-list/wish-list.module#WishListPageModule"
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsRoutingModule {}
