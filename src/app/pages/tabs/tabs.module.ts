import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";

import { TabsRoutingModule } from "./tabs-routing.module";
import { TabsPage } from "./tabs.page";
// import { Tab1PageModule } from './../tab1/tab1.module';
// import { Tab2PageModule } from './../tab2/tab2.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsRoutingModule
    // Tab1PageModule,
    // Tab2PageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}

// Not needed anymore since beta 18 and lazy loading!
