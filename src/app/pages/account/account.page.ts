import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { ProfileComponent } from "./profile/profile.component";
@Component({
  selector: "app-account",
  templateUrl: "./account.page.html",
  styleUrls: ["./account.page.scss"]
})
export class AccountPage implements OnInit {
  constructor(public modalController: ModalController) {}

  ngOnInit() {}

  async goProfile() {
    const modal = await this.modalController.create({
      component: ProfileComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }
}
