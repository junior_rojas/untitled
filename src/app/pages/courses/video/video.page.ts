// https://www.w3schools.com/howto/rain.mp4

import { Component, OnInit, ViewChild } from "@angular/core";
import { VideoOptions, VideoPlayer } from "@ionic-native/video-player/ngx";
import { ScreenOrientation } from "@ionic-native/screen-orientation/ngx";
import { LoadingController } from "@ionic/angular";
import { Router } from "@angular/router";

@Component({
  selector: "app-video",
  templateUrl: "./video.page.html",
  styleUrls: ["./video.page.scss"]
})
export class VideoPage implements OnInit {
  public loading: any;
  public _videoOptions: VideoOptions;
  public _videoUrl: string;
  public textHiddden = true;
  @ViewChild("videoPlayer") mVideoPlayer: any;

  constructor(
    public _videoPlayer: VideoPlayer,
    public screenOrientation: ScreenOrientation,
    public _lc: LoadingController,
    private router: Router
  ) {}

  async ngOnInit() {
    this.playVideo();
    // this.setLandscapeOrientation();
  }

  async playVideo() {
    try {
      let video = this.mVideoPlayer.nativeElement;
      video.src = this._videoUrl;
      video.play();

      this._videoOptions = {
        volume: 0.7
      };

      this._videoUrl =
        "https://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
      await this._videoPlayer.play(this._videoUrl, this._videoOptions);
    } catch (error) {
      console.log(error);
    }
  }

  


  stopVideo() {
    this._videoPlayer.close();
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.setUnlockOrientation();
    this.router.navigateByUrl("/pages/playlist");
  }

  getCurrentOrientation() {
    console.log(this.screenOrientation.type);
  }

  setLandscapeOrientation() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
  }

  setUnlockOrientation() {
    this.screenOrientation.unlock();
  }

  detectOrientationChanges() {
    this.screenOrientation.onChange().subscribe(() => {
      console.log("Orientation Changed");
    });
  }
}
