// https://www.techiediaries.com/inappbrowser-ionic-v3/
import { Component, OnInit } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { Router } from "@angular/router";

import {
  PayPal,
  PayPalPayment,
  PayPalConfiguration
} from "@ionic-native/paypal/ngx";

import {
  AuthService,
  CartService,
  PayuService,
  PaypalService,
  CommonService
} from "../../services/services";

import {
  InAppBrowser,
  InAppBrowserOptions,
  InAppBrowserEvent
} from "@ionic-native/in-app-browser/ngx";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.page.html",
  styleUrls: ["./cart.page.scss"]
})
export class CartPage implements OnInit {
  public selectedItems = [];
  public total = 0;
  public _browser: any;
  public options: InAppBrowserOptions = {
    location: "yes", //Or 'no'
    hidden: "no", //Or  'yes'
    clearcache: "yes",
    clearsessioncache: "yes",
    zoom: "yes", //Android only ,shows browser zoom controls
    hardwareback: "yes",
    mediaPlaybackRequiresUserAction: "no",
    shouldPauseOnSuspend: "no", //Android only
    closebuttoncaption: "Close", //iOS only
    disallowoverscroll: "no", //iOS only
    toolbar: "yes", //iOS only
    enableViewportScale: "no", //iOS only
    allowInlineMediaPlayback: "no", //iOS only
    presentationstyle: "pagesheet", //iOS only
    fullscreen: "yes" //Windows only
  };
  public paymentString: string;
  public urlRoot;
  constructor(
    public db: AngularFireDatabase,
    public authService: AuthService,
    private payPal: PayPal,
    private theInAppBrowser: InAppBrowser,
    public _cs: CommonService,
    private cartService: CartService,
    public _payu: PayuService,
    public _paypal: PaypalService,
    private router: Router
  ) {}

  ngOnInit() {
    this.urlRoot = `users/${this.authService.token}`;
    let items = this.cartService.getCart();
    let selected = {};
    for (let obj of items) {
      if (selected[obj.id]) {
        selected[obj.id].count++;
      } else {
        selected[obj.id] = { ...obj, count: 1 };
      }
    }
    this.selectedItems = Object.keys(selected).map(key => selected[key]);
    this.total = this.selectedItems.reduce((a, b) => a + b.count * b.price, 0);
  }

  public async payu() {
    const signature = await this._payu.md5(this.total);
    this.paymentString = `
    <form id="payu_form" method="post" action="${this._payu.actionURL}" >
        <input name="merchantId"    type="hidden"  value="508029"   >
        <input name="accountId"     type="hidden"  value="512321" >
        <input name="description"   type="hidden"  value="Test PAYU"  >
        <input name="referenceCode" type="hidden"  value="TestPayU" >
        <input name="amount"        type="hidden"  value="20000"   >
        <input name="tax"           type="hidden"  value="0"  >
        <input name="taxReturnBase" type="hidden"  value="0" >
        <input name="currency"      type="hidden"  value="COP" >
        <input name="extra1"        type="hidden"  value="${
          this.authService.token
        }" >
        <input name="signature"     type="hidden"  value="7ee7cf808ce6a39b17481c54f2c57acc"  >
        <input name="test"          type="hidden"  value="1" >
        <input name="buyerEmail"    type="hidden"  value="test@test.com" >
        <input name="responseUrl"    type="hidden"  value="${
          this._payu.responseUrl
        }" >
        <input name="confirmationUrl"    type="hidden"  value="${
          this._payu.confirmationUrl
        }" >
        <button type="submit" value="submit" #submitBtn></button>
    </form>
    <script type="text/javascript">document.getElementById("payu_form").submit();</script>

    `;
    console.log(1, this.paymentString);
    console.log(2, signature);
    this.paymentString = "data:text/html;base64," + btoa(this.paymentString);

    let target = "_blank";
    this._browser = await this.theInAppBrowser.create(
      this.paymentString,
      target,
      this.options
    );
    this.purchaseStatus();
  }

  async paypal() {
    this.payPal
      .init({
        PayPalEnvironmentProduction: this._paypal.payPalEnvironmentProduction,
        PayPalEnvironmentSandbox: this._paypal.payPalEnvironmentSandbox
      })
      .then(
        () => {
          // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
          this.payPal
            .prepareToRender(
              "PayPalEnvironmentSandbox",
              new PayPalConfiguration({
                // Only needed if you get an "Internal Service Error" after PayPal login!
                //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
              })
            )
            .then(
              () => {
                let payment = new PayPalPayment(
                  "3.33",
                  "USD",
                  "Description",
                  "sale"
                );
                this.payPal.renderSinglePaymentUI(payment).then(
                  async () => {
                    this.setPurchase("paypal", payment);
                    this._cs.presentToast("Successfully paid");
                  },
                  () => {
                    this._cs.presentToast(
                      "Error or render dialog closed without being successful"
                    );
                  }
                );
              },
              () => {
                this._cs.presentToast("Error in configuration");
              }
            );
        },
        () => {
          this._cs.presentToast(
            "Error in initialization, maybe PayPal isn't supported or something else"
          );
        }
      );
  }

  async setPurchase(url, data) {
    console.log(1, url);
    console.log(2, JSON.stringify(data));
    console.log(3, `${this.urlRoot}/purchase/${url}`);
    const purchaseRef = this.db.list(`${this.urlRoot}/purchase/${url}`);
    this.openVideo();
    await purchaseRef.push(data);
    this.openVideo();
  }

  purchaseStatus() {
    this.db
      .object(`${this.urlRoot}/confirmationPayu`)
      .valueChanges()
      .subscribe((data: any) => {
        if (data) {
          if (data.response_message_pol == "APPROVED") {
            this._cs.presentToast("Successfully paid");
            this.browserClose();
            this.openVideo();
            return;
          } else {
            this._cs.presentToast(data.response_message_pol);
            this.browserClose();
          }
        }
      });
  }

  openVideo() {
    console.log(4, "entro");
    this.router.navigateByUrl("/pages/playlist");
  }

  browserClose() {
    this._browser.close();
  }
}
