import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { ChatPage } from "./chat.page";
import { EmojiPickerComponent } from "./emoji-picker/emoji-picker.component";
import { RelativeTimePipe } from "../../pipes/RelativeTime/relative-time.pipe";

const routes: Routes = [
  {
    path: "",
    component: ChatPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChatPage, EmojiPickerComponent, RelativeTimePipe],
  entryComponents: [EmojiPickerComponent]
})
export class ChatPageModule {}
