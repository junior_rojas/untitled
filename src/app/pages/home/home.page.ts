import { Component } from "@angular/core";
import { CartService } from "../../services/cart/cart.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  public cart = [];
  constructor(private router: Router, private cartService: CartService) {}

  ngOnInit() {
    this.cart = this.cartService.getCart();
  }

  openCart() {
    this.router.navigateByUrl("/pages/cart");
  }
}
