import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-main-course",
  templateUrl: "./main-course.component.html",
  styleUrls: ["./main-course.component.scss"]
})
export class MainCourseComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  openCourseDetails() {
    const r = { id: 0, name: "Lesson 1", price: "80000" };
    this.router.navigate(["/pages/course-details", JSON.stringify(r)]);
  }
}
