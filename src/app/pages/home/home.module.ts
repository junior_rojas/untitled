import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { HomePage } from "./home.page";
import { MainCourseComponent } from "./main-course/main-course.component";
import { CollapseCourseComponent } from "./collapse-course/collapse-course.component";
import { MostViewedCourseComponent } from "./most-viewed-course/most-viewed-course.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: "",
        component: HomePage
      }
    ])
  ],
  declarations: [
    HomePage,
    MainCourseComponent,
    CollapseCourseComponent,
    MostViewedCourseComponent
  ],
  entryComponents: [
    MainCourseComponent,
    CollapseCourseComponent,
    MostViewedCourseComponent
  ]
})
export class HomePageModule {}
