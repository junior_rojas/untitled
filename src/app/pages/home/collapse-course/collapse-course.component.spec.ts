import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollapseCourseComponent } from './collapse-course.component';

describe('CollapseCourseComponent', () => {
  let component: CollapseCourseComponent;
  let fixture: ComponentFixture<CollapseCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollapseCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapseCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
