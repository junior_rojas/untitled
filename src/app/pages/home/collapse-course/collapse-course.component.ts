import { Component, OnInit } from "@angular/core";
import { CartService } from "../../../services/cart/cart.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-collapse-course",
  templateUrl: "./collapse-course.component.html",
  styleUrls: ["./collapse-course.component.scss"]
})
export class CollapseCourseComponent implements OnInit {
  public items = [];
  public sliderConfig = {
    slidesPerView: 1.6,
    spaceBetween: 10,
    centeredSlides: true
  };
  constructor(private cartService: CartService, private router: Router) {}

  ngOnInit() {
    this.items = this.cartService.getProducts();
  }

  addToCart(product) {
    // this.cartService.addProduct(product);
    console.log(product)
    this.router.navigate(["/pages/course-details", JSON.stringify(product)]);
  }
}
