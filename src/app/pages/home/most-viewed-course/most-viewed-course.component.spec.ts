import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostViewedCourseComponent } from './most-viewed-course.component';

describe('MostViewedCourseComponent', () => {
  let component: MostViewedCourseComponent;
  let fixture: ComponentFixture<MostViewedCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostViewedCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostViewedCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
