import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", loadChildren: "./tabs/tabs.module#TabsPageModule" },
  {
    path: "video",
    loadChildren: "./courses/video/video.module#VideoPageModule"
  },
  {
    path: "playlist",
    loadChildren: "./courses/playlist/playlist.module#PlaylistPageModule"
  },
  {
    path: "cart",
    loadChildren: "./cart/cart.module#CartPageModule"
  },
  {
    path: "course-details/:data",
    loadChildren:
      "./course-details/course-details.module#CourseDetailsPageModule"
  },
  { path: "chat", loadChildren: "./chat/chat.module#ChatPageModule" },
  { path: 'notifications', loadChildren: './notifications/notifications.module#NotificationsPageModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
