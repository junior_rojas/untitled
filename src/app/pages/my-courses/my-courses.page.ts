import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-my-courses",
  templateUrl: "./my-courses.page.html",
  styleUrls: ["./my-courses.page.scss"]
})
export class MyCoursesPage implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  openPlaylist() {
    this.router.navigateByUrl("/pages/playlist");
  }
}
