import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TutorialGuard } from "./guards/tutorial/tutorial.guard";
import { AuthGuardService } from "./guards/auth/auth-guard.service";
const routes: Routes = [
  { path: "", redirectTo: "auth", pathMatch: "full" },
  {
    path: "auth",
    loadChildren: "./public/auth/auth.module#AuthPageModule",
    canActivate: [TutorialGuard]
  },
  {
    path: "pages",
    loadChildren: "./pages/pages-routing.module#PagesRoutingModule",
    canActivate: [AuthGuardService]
  },
  {
    path: "tutorial",
    loadChildren: "./public/tutorial/tutorial.module#TutorialPageModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
