import { Component } from "@angular/core";
import { AuthService, ThemeSwitcherService } from "../../../services/services";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { MenuController } from "@ionic/angular";
import { AngularFireDatabase } from "@angular/fire/database";

@Component({
  selector: "app-sidebar-menu",
  templateUrl: "./sidebar-menu.component.html",
  styleUrls: ["./sidebar-menu.component.scss"]
})
export class SidebarMenuComponent {
  appMenuItems: any;
  profile: any;
  _profile;
  interval;
  timeLeft: number;
  isChecked;
  constructor(
    public _auth: AuthService,
    public themeSwitcher: ThemeSwitcherService,
    private router: Router,
    private menu: MenuController,
    private storage: Storage,
    public db: AngularFireDatabase
  ) {
    this.appMenuItems = [
      { title: "Home", icon: "home", router: "/pages/tabs/home" },
      {
        title: "Chat",
        icon: "chatboxes",
        router: "/pages/chat"
      },
      {
        title: "Notificaciones",
        icon: "list-box",
        router: "/pages/notifications"
      },
      {
        title: "Certificados",
        icon: "podium",
        router: "/pages/tabs/account"
      },
      {
        title: "Historial de pago",
        icon: "card",
        router: "/pages/tabs/home"
      }
    ];
    this.stopwatch();
  }

  darkMode() {
    if (this.isChecked) {
      this.themeSwitcher.setTheme("night");
    } else {
      this.themeSwitcher.setTheme("day");
    }
  }

  stopwatch() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.getProfile();
      }
    }, 1000);
  }

  pauseTimer() {
    clearInterval(this.interval);
  }

  getProfile() {
    this.storage.get("profile").then(res => {
      if (res) {
        this.profile = res;
        this.pauseTimer();
      }
      return res;
    });
  }

  go(r) {
    this.router.navigateByUrl(r);
    this.menu.close();
  }

  logout() {
    this._auth.logout();
  }
}
