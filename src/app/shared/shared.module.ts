import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";

import { SidebarMenuComponent } from "./components/sidebar-menu/sidebar-menu.component";
import { FormsModule } from "@angular/forms";
@NgModule({
  declarations: [SidebarMenuComponent],
  imports: [BrowserModule, CommonModule, FormsModule, IonicModule],
  providers: [],
  exports: [SidebarMenuComponent]
})
export class SharedModule {}
